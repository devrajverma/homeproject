from django.contrib import admin
from homeapp.models import( Contact_Us,Category,register_table,addproperty,add_city,cart,Order)

admin.site.site_header="Hommy.Com"
# Register your models here.
class Contact_UsAdmin(admin.ModelAdmin):
    fields = ["name","email","message"]
    list_display = [ "id","name","email","message","added_on"]
    search_fields = ["name"]
    list_filter = ["added_on","name"]
    list_editable = ["name"]
class CategoryAdmin(admin.ModelAdmin):

    list_display = ["id","cat_name","description","added_on"]

admin.site.register(Contact_Us,Contact_UsAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(register_table)
admin.site.register(add_city)
admin.site.register(addproperty)
admin.site.register(cart)
admin.site.register(Order)