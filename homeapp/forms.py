from django import forms
from  homeapp.models import addproperty

class add_property_form(forms.ModelForm):
      class Meta:
          model= addproperty
          fields="__all__"
          exclude = ["owner","area"]
         