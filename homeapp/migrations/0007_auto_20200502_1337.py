# Generated by Django 3.0.4 on 2020-05-02 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homeapp', '0006_auto_20200502_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addproperty',
            name='area',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='is_Gym',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='is_TV_cable',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='is_air_conditioning',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='is_laundry_room',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='is_wifi',
            field=models.BooleanField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='no_of_bathrooms',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='addproperty',
            name='no_of_bedrooms',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
