from django.db import models
from django.contrib.auth.models import User
import datetime

class Contact_Us(models.Model):
    name=models.CharField(max_length=250) 
    email=models.EmailField(max_length=200) 
    message= models.TextField()
    added_on =models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name
    class meta:
        verbose_name_plural="Contact"

class Category(models.Model):
    cat_name = models.CharField(max_length=250)  
    cover_pic = models.FileField(upload_to="media/%y/%m/%d") 
    description = models.TextField()
    added_on =models.DateTimeField(auto_now_add=True)

    
    def __str__(self):
        return self.cat_name 
class register_table(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number= models.IntegerField()
    profile_pic = models.ImageField(upload_to = "profiles/%y/%m/%d", null=True)
    age = models.CharField(max_length = 250, null=True, blank=True)
    city = models.CharField(max_length=250, null=True,blank=True)
    about = models.TextField(blank=True,null=True)
    gender= models.CharField(max_length=250)
    occupation = models.CharField(max_length=250,null=True, blank=True)
    added_on =models.DateTimeField(auto_now_add=True , null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.user.username     

class addproperty(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    property_name = models.CharField(max_length=250,blank=True)
    price = models.FloatField(blank=True)
    booking_amount = models.FloatField(blank=True)
    property_image= models.ImageField(upload_to = "profiles/%y/%m/%d", null=True ,blank=True)
   # city= models.ForeignKey(add_city,on_delete=models.CASCADE)
    property_category = models.ForeignKey(Category,on_delete=models.CASCADE)
    property_status = models.CharField(max_length=250,blank=True)
    area=models.FloatField(blank=True,null=True)
    no_of_bedrooms=models.IntegerField(blank=True,null=True)
    no_of_bathrooms=models.IntegerField(blank=True,null=True)
    is_air_conditioning = models.BooleanField(blank=True,null=True)
    is_Gym = models.BooleanField(blank=True,null=True)
    is_laundry_room = models.BooleanField(blank=True,null=True)
    is_TV_cable = models.BooleanField(blank=True,null=True)
    is_wifi = models.BooleanField(blank=True,null=True)
    description = models.TextField(blank=True,null=True)
    added_on = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
       return self.property_name

class add_city(models.Model):
    city_name= models.CharField(max_length=250)
    pin_code= models.IntegerField()
    state = models.CharField(max_length=250)
    country = models.CharField(max_length=250)
    added_on = models.DateTimeField()
    def __str__(self):
        return self.city_name

class cart(models.Model):
      user=models.ForeignKey(User,on_delete=models.CASCADE)  
      property=models.ForeignKey(addproperty,on_delete=models.CASCADE) 
      Quantity=models.IntegerField()
      status=models.BooleanField(default=False)
      added_on =models.DateTimeField(auto_now_add=True,null=True)
      update_on = models.DateTimeField(auto_now=True,null=True) 

      def __str__(self):
         return self.user.username    


class Order(models.Model):
    cust_id = models.ForeignKey(User,on_delete=models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    property_ids= models.CharField(max_length=250)
    invoice_id= models.CharField(max_length=250)
    status= models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.cust_id.username          