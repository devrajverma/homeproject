from django.shortcuts import render , get_object_or_404, reverse
from  django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from homeapp.models import Contact_Us, Category ,register_table,addproperty,cart,Order
from  django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from homeapp.forms import add_property_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage

def index(request):
        return render(request,"base.html")

def home(request):
    if "user_id" in request.COOKIES:
        uid= request.COOKIES["user_id"]
        usr=get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
             return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/cust_dashboard") 
   
    recent = Contact_Us.objects.all().order_by("-id")[:5]
    cats= Category.objects.all().order_by("cat_name")
    
    return render(request,"homepage.html",{"messages":recent,"Category":cats})

def about(request):
     cats= Category.objects.all().order_by("cat_name")
     return render(request,"about us.html",{"Category":cats})
    
def add(request):
    
     context={}
     ch=register_table.objects.filter(user_id=request.user.id)
     if len(ch)>0:
       data=register_table.objects.get(user_id=request.user.id)
       context["data"]= data
     form = add_property_form()
     if request.method=="POST":
         form =  add_property_form(request.POST,request.FILES)
         if form.is_valid():
             data = form.save(commit=False)
             login_user= User.objects.get(username=request.user.username)
             data.owner= login_user
             data.save()
             context["satatus"]="{} Added Successfully!!". format(data.property_name)
     context["form"]= form
     
     return render(request,"addproperty.html",context)
    
def all(request):
    context={}
    cats= Category.objects.all().order_by("cat_name")
    context["Category"] = cats
    all_property=addproperty.objects.all().order_by("property_name")
    context["property"]=all_property
    if "qry" in request.GET:
        q = request.GET["qry"]
        prd=addproperty.objects.filter(property_name__icontains=q)
        context["property"]=prd
        
    if "cat" in request.GET:
        cid= request.GET["cat"]
        prd = addproperty.objects.filter(property_category__id=cid)
        context["property"]=prd
        
    return render(request,"allproperties.html",context)
    
def change(request):
    context={}
    ch=register_table.objects.filter(user_id=request.user.id)
    
    if len(ch)>0:
       data=register_table.objects.get(user_id=request.user.id)
       context["data"]= data
    if request.method=="POST":
       current = request.POST["cpwd"] 
       new_pas = request.POST["npwd"] 

       user = User.objects.get(id=request.user.id)
       un= user.username
      
       check = user.check_password(current)
       

       if check==True:
          user.set_password(new_pas)
          user.save()
          
          context["msz"] = "password change successfully!!"
          context["col"] = "alert-success"
          user=User.objects.get(username=un)
          login(request,user)
       else:
            context["msz"] = "incorrect old password"
            context["col"] = "alert-danger"
            

    return render(request,"chnge.html",context)
    
def user_login(request):
     if request.method== "POST":
         un=request.POST["username"]
         pwd= request.POST["password"]
         user= authenticate(username=un, password=pwd)
         if user:
             login(request, user)
             if user.is_superuser:
                 return HttpResponseRedirect("/admin")
             else:
                 res= HttpResponseRedirect("/cust_dashboard")
                 if "rememberme" in request.POST:
                     res.set_cookie("user_id",user.id)
                     res.set_cookie("date_login",datetime.now())
                 return res
            # if user.is_active:
             #      return HttpResponseRedirect("/cust_dashboard")
         else:
            return render(request, "user_login.html",{"status": "invalid username or password"})
                  
     return render(request,"user_login.html")            
@ login_required    
def cust_dashboard(request):
      context= {}
      check=register_table.objects.filter(user_id=request.user.id) 
      if len(check)>0:
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]=data
      return render(request, "cust_dashboard.html",context)
@ login_required    
def seller_dashboard(request):
     return render(request, "seller_dashboard.html")  
@ login_required 
def user_logout(request):
    logout(request) 
    res= HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return (res)
 
def myproperty(request):
    context={}
    check=register_table.objects.filter(user_id=request.user.id) 
    if len(check)>0:
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]=data
        all= addproperty.objects.filter(owner_id=request.user.id).order_by("-id")
        context["property"]=all
    return render(request,"myproperty4.html",context)
    
def myprofile(request):
     context={}
     check=register_table.objects.filter(user_id=request.user.id) 
     if len(check)>0:
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]=data
  
     return render(request,"myprofile1.html", context)
    
def contact(request):
    all_data = Contact_Us.objects.all().order_by("-id") 
    cats= Category.objects.all().order_by("cat_name")
    print(all_data)
    if request.method == "POST":
       nm = request.POST["name"]
       email = request.POST["email"]
       msz = request.POST["message"]
        
       data= Contact_Us(name = nm, email = email,message = msz) 
       data.save()
       res = "dear{} thanks for your feedback".format(nm)
       return render (request,"contact us.html",{"status":res,"messages":all_data,"Category":cats})
      # return HttpResponse("<h1 style='color:green;'>Data Saved Successfully!</h1>")
     
    return render(request,"contact us.html",{"messages":all_data,"Category":cats})
  
    
def sign(request):
    if "user_id" in request.COOKIES:
        uid= request.COOKIES["user_id"]
        usr=get_object_or_404(User,id=uid)
        login(request,usr)
        if usr.is_superuser:
             return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/cust_dashboard") 
    if request.method=="POST":
         fname= request.POST["first"]
         last=request.POST["last"]
         un=request.POST["uname"]
         pwd= request.POST["Password"]
         email=request.POST["email"]
         con=request.POST["contact"]
         tp= request.POST["utype"]

         usr = User.objects.create_user(un,email,pwd)
         usr.first_name=fname
         usr.last_name= last
         if tp == "sell":
             usr.is_staff=True  
         usr.save()
          
         reg = register_table(user=usr , contact_number=con)
         reg.save()
        
         
         return render(request,"sign up.html",{"status":"Mr/Miss.{} Account create Successfully!".format(fname)}) 
        # return HttpResponse("register successfully!!")
         
    return render(request,"sign up.html")

        
def check_user(request):
    if request.method=="GET":
        un=request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check)==1:
            return HttpResponse("exists")
        else:
            return HttpResponse("not exists") 
            
def edit_profile(request):
     context={}
     check= register_table.objects.filter(user_id=request.user.id)
     if len(check)>0: 
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]= data
     if request.method =="POST":
         print(request.POST)
         fn=request.POST["fname"]
         ln=request.POST["lname"]
         em=request.POST["email"]
         con=request.POST["contact"]
         city = request.POST["city"]
         age=request.POST["age"]
         gen=request.POST["gender"]
         occ=request.POST["occ"]
         abt=request.POST["about"]

         usr = User.objects.get(id=request.user.id)
         usr.first_name = fn
         usr.lirst_name = ln
         usr.email = em
         usr.save()

         data.contact_number=con
         data.age = age
         data.city = city
         data.gender=  gen
         data.occupation = occ
         data.about = abt
         data.save()
         if"image" in request.FILES:
             img = request.FILES["image"]
             data.profile_pic = img
             data.save()
             context["status"]="changes saved Successfully"
         
     return render(request,"edit_profile.html", context)                   

    
def single(request):
    context={}
    pid = request.GET["pid"]
    obj=addproperty.objects.get(id=pid)
    context["property"]=obj
    
    
    return render(request,"singleproperty.html",context)
  
def update_property(request):
     context={}
     cats= Category.objects.all().order_by("cat_name")
     context["Category"] = cats
     pid= request.GET["pid"]
     property=addproperty.objects.get(id=pid)
     context["property"] = property
     if request.method =="POST":
        pn=request.POST["pname"]
      #  ct_id = request.POST["pcat"]
        pr = request.POST["pp"]
        sp = request.POST["sp"]
        des = request.POST["des"]
        print(request.POST)
       # cat_obj= Category.objects.get(id=ct_id)
        property.property_name=pn
       # property.property_category=cat_obj
        property.price=pr
        property.booking_amount=sp
        property.description=des
        if "pimg" in request.FILES:
            img = request.FILES ["pimg"]
            property.property_image = img
            property.save()
            context["status"] = "changes saved successfully!!"
            context["id"] = pid
     return render(request,"update.html",context)
def delete_property(request):
    context = {}
    if "pid" in request.GET:
        pid = request.GET["pid"]
        prd =addproperty.objects.get(id=pid)
        context["property"] = prd

        if"action" in request.GET:
            prd.delete()
            context["status"] = str(prd.property_name)+"Deleted Successfully!!"
    return render(request,"delete.html",context)    



def sendemail(request):
    context={}
    check=register_table.objects.filter(user_id=request.user.id) 
    if len(check)>0:
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]=data
 
    if request.method=="POST":
     
         rec = request.POST["to"].split(",")
         print(rec)
         sub = request.POST["sub"]
         msz = request.POST["msz"]
 
         try:
            em = EmailMessage(sub,msz,to=rec)
            em.send()
            context["status"] = "Email Sent"
            context["cls"] = "alert-primary"
         except:
            context["status"] = "Could not Send, Please check Internet Connection / Email Address"
            context["cls"] = "alert-danger"
      
    return render(request,"sendemail.html",context)    
def forgotpass(request):
     context = {}
     if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/cust_dashboard")
       # context["status"] = "Password Changed Successfully!!!"

     return render(request,"forgot_pass.html",context)


import random

def reset_password(request):
     un = request.GET["username"]
     try:
        user = get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your One Time Password (OTP) \nDo not share it with others \nThanks&Regards \nHommy.Com".format(user.username, otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})
     except:
        return JsonResponse({"status":"failed"})

def add_to_cart(request):
    context={}
    item=cart.objects.filter(user__id=request.user.id,status=False)
    context["item"]=item
    if request.user.is_authenticated:
       if request.method=="POST":
           pid=request.POST["pid"]
           qty=request.POST["qty"]
           is_exist= cart.objects.filter(property_id=pid, user__id=request.user.id,status=False)
           if len(is_exist)>0:
               context["msz"]= "Item Alreddy  Exist in your Cart"
               context["cls"]= "alert alert-warning"
           else:
               property=get_object_or_404(addproperty,id=pid)
               usr= get_object_or_404(User, id=request.user.id)
               c=cart(user=usr,property=property,Quantity=qty)
               c.save()
               context["msz"]= "{} Added in Your Cart".format(property.property_name)
               context["cls"]= "alert alert-success"
        
    else:
        context["status"]="Please Login To View Cart"    
    return render(request,"cart.html",context)   
def get_cart_data(request):
    items= cart.objects.filter(user__id=request.user.id, status=False)
    sale,total,Quantity=0,0,0
    for i in items:
        sale+= float(i.property.booking_amount)
        total+=float(i.property.price)
        Quantity+=float (i.Quantity)

    res= {
        "total":total,"booking_amount":sale,"quan":Quantity,

    }   
    return JsonResponse(res) 

def change_quan(request):

    if "delete_cart" in request.GET:
        id= request.GET["delete_cart"]
        cart_obj= get_object_or_404(cart,id=id)
        cart_obj.delete()

        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
def process_payment(request):
    items= cart.objects.filter(user__id=request.user.id, status=False)
    property=""
    amt=0
    inv="INV-"
    cart_ids=""
    p_ids=""
    for j in items:
        property= str(j.property.property_name)+"\n"
        p_ids= str(j.property_id)+","
        amt+= float(j.property.booking_amount)  
        inv+= str(j.id)  
        cart_ids+= str(j.id)+","

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': 'property',
        'invoice': 'inv',
        
        'notify_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('paypal-ipn')),
         'return_url': 'http://{}{}'.format("127.0.0.1:8000",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("127.0.0.1:8000",
                                              reverse('payment_cancelled')),                                  

    }
    usr= User.objects.get(username=request.user.username)
    ord = Order(cust_id=usr,cart_ids =cart_ids,property_ids=p_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"]= ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})     

def payment_done(request):
    if "order_id" in request.session:
        order_id= request.session["order_id"]
        ord_obj= get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()

        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status = True
            cart_object.save()
    return render(request,"payment_success.html")


def payment_cancelled(request):
    return render(request,"Payment_failed.html")    

def order_history(request):
    context={}
    check=register_table.objects.filter(user_id=request.user.id) 
    if len(check)>0:
        data=register_table.objects.get(user_id=request.user.id)
        context["data"]=data
    all_order = []    
    orders= Order.objects.filter(cust_id__id=request.user.id).order_by("-id")
    for order in orders: 
        property = [] 
        for id in order.property_ids.split(",")[:-1]:
            pro = get_object_or_404(addproperty, id=id) 
            property.append(pro)
        ord = {
            "order_id":order.id,
            "property":property,
            "invoice": order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }    
        all_order.append(ord)
        context["order_history"]=all_order
    return render(request,"order_history.html",context)